# -*- coding: utf-8 -*-
#!/usr/bin/env python

import pika

#establish connection
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()
#receive queue- hello queue
channel.queue_declare(queue='hello')
#exchange
channel.basic_publish(exchange='',
                      routing_key='hello',
                      body = 'Hello World!')
print(" [x] Sent 'Hello World!'")
connection.close()