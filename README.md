本次项目用以模拟数据收集，通过发布订阅传输数据并显示。

rabbitmq-weatherdata项目文件

> data : 数据集
>
> sense: 功能实现文件
>
> > display.py: 图像显示及消息接受函数综合
> >
> > emit_logs_direct.py: 消息发送
> >
> > receive_logs_direct.py：仅消息接收
>
> Rabbitmq:练习示例

操作过程：

1. 先运行display.py

创建—>选择—>绘图—>清除—>创建

2. 然后运行emit_logs_direct.py传输数据(点击绘图后)

最终结果如图：

![](picture/1.png)