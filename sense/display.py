"""
    画布文件，实现绘图区域的显示，并返回画布的对象。
"""
import ctypes
import inspect
import tkinter as tk
import numpy as np
import tkinter.messagebox as mb

# 创建画布需要的库
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
# 创建工具栏需要的库
from matplotlib.backends.backend_tkagg import NavigationToolbar2Tk
# 快捷键需要的库
from matplotlib.backend_bases import key_press_handler
# 导入画图常用的库
from matplotlib.figure import Figure

import pika, sys, os
import threading

#记录线程id
global tid

def plot_fun(root):
    """
        该函数实现的是内嵌画布,不负责画图,返回画布对象。
    :param root:父亲控件对象, 一般是容器控件或者窗体
    :return: 画布对象
    """
    # 画布的大小和分别率
    fig = Figure(dpi=100)
    axs = fig.add_subplot(111)

    # 创建画布
    canvas = FigureCanvasTkAgg(fig, master=root)  # A tk.DrawingArea.
    canvas.draw()
    # 显示画布
    canvas.get_tk_widget().pack()

    # 创建画图工具条（默认隐藏）
    toolbar = NavigationToolbar2Tk(canvas, root)
    toolbar.update()
    # 显示画图工具条
    canvas.get_tk_widget().pack()

    # 调用快捷键
    def on_key_press(event):
        key_press_handler(event, canvas, toolbar)

    canvas.mpl_connect("key_press_event", on_key_press)

    # 返回画布的对象
    return axs

"""
    绘图文件，负责函数曲线的绘制
"""


def plot_main(plt):
    """
        负责函数曲线的绘制
    :param routing_keysion: 数据列表
    :param plt: 画布的对象
    :return: 无
    """
    # list_expr = []
    # list_expr = string.split(",")
    # string1 = []
    # for sub_expr in list_expr:
    #     string1.append(sub_expr)
    # x = np.linspace(-10, 10, 100)
    # y = []
    # num = string.count('x')
    # for i in x:
    #     t = (i, ) * num
    #     string = string.replace("x", "(%f)")
    #     i = eval(string % t)
    #     y.append(i)
    # plt.plot(x, y)
    # plt.grid(True)
    # plt.legend(labels=string1)

"""
    控件文件，负责程序控件的创建与布局
"""

def widget_main(win, root):
    """
        负责程序控件的创建与布局
    :param win: 主窗体的对象。
    :param root: 绘图区的容器对象。
    :return: 无
    """
    # 控件区的容器对象
    frame1 = None


# ===========功能区============================
    # 绘图的功能函数
    def plot_f():
        # string = entry.get()
        # # 判断输入框是否为空
        # if string == "":
        #     mb.showerror("提示", "没有输入值，请重新输入：")
        # else:
        #     # 判断是否已经创建画布
        #     if frame1==None:
        #         mb.showerror("提示", "没有创建画布，不能画图，请先创建画布")
        #     else:
        #         axs = plot_fun(frame1)
        #         plot_main(string, axs)
        if len(routing_keys) == 1:
            mb.showerror("提示","未选择数据")
        else:
            # 判断是否已经创建画布
            if frame1==None:
                mb.showerror("提示", "没有创建画布，不能画图，请先创建画布")
            else:
                #清空上一次选择的数据
                dict.clear()
                date.clear()
                axs = plot_fun(frame1)  #创建画布
                main(axs)   #开始接收消息

    # 清除的功能函数
    def clear():
        nonlocal frame1
        if frame1==None:
            mb.showerror("提示", "已经没有画布，无法清除画布")
        else:
            frame1.destroy()
            frame1 = None

    # 创建画布的功能函数
    def create():
        nonlocal frame1
        if frame1 != None:
            mb.showerror("提示", "画布已经存在，请不要重复创建画布")
        else:
            frame1 = tk.LabelFrame(win, bg="#F5F5DC", text="画-----布", labelanchor="n", fg="blue")
            frame1.place(relx=0.00, rely=0.05, relwidth=0.62, relheight=0.95)

    #功能选择
    def select():
        if (var1.get() == 1) and ('rain' not in routing_keys):
            routing_keys.append('rain')
        elif (var1.get() == 0) and ('rain' in routing_keys):
            routing_keys.remove('rain')
        if (var2.get() == 1) and ('humidity' not in routing_keys):
            routing_keys.append('humidity')
        elif (var2.get() == 0) and ('humidity' in routing_keys):
            routing_keys.remove('humidity')
        if (var3.get() == 1) and ('pressure' not in routing_keys):
            routing_keys.append('pressure')
        elif (var3.get() == 0) and ('pressure' in routing_keys):
            routing_keys.remove('pressure')
        if (var4.get() == 1) and ('sunshine' not in routing_keys):
            routing_keys.append('sunshine')
        elif (var4.get() == 0) and ('sunshine' in routing_keys):
            routing_keys.remove('sunshine')
        if (var5.get() == 1) and ('temperature' not in routing_keys):
            routing_keys.append('temperature')
        elif (var5.get() == 0) and ('temperature' in routing_keys):
            routing_keys.remove('temperature')
        print(routing_keys)


    # =============控件区======================
    #  标签控件
    label = tk.Label(root,
                     text="请选择数据类型：",
                     bg = '#808080',
                     font=("微软雅黑", 15),
                     fg='white')
    label.place(relx=0.1, rely=0.12)

    #勾选区
    var1 = tk.IntVar()
    var2 = tk.IntVar()
    var3 = tk.IntVar()
    var4 = tk.IntVar()
    var5 = tk.IntVar()

    c1 = tk.Checkbutton(root, text='rain', variable=var1, onvalue=1, offvalue=0,command=select)
    c2 = tk.Checkbutton(root, text='humidity', variable=var2, onvalue=1, offvalue=0,command=select)
    c3 = tk.Checkbutton(root, text='pressure', variable=var3, onvalue=1, offvalue=0,command=select)
    c4 = tk.Checkbutton(root, text='sunshine', variable=var4, onvalue=1, offvalue=0,command=select)
    c5 = tk.Checkbutton(root, text='temperature', variable=var5, onvalue=1, offvalue=0,command=select)

    c1.place(relx=0.1, rely=0.2)
    c2.place(relx=0.1, rely=0.25)
    c3.place(relx=0.1, rely=0.3)
    c4.place(relx=0.1, rely=0.35)
    c5.place(relx=0.1, rely=0.4)


    # # 输入框
    # entry = tk.Entry(root, font=("华文楷体", 15))
    # entry.place(relx=0.1, rely=0.2, relwidth=0.8)

    # 创建画布区
    btn_draw = tk.Button(root,
                         text="创建",
                         cursor="hand2",
                         width=10,
                         bg="white",
                         relief="raised",
                         command=create
                         )
    btn_draw.place(relx=0.1, rely=0.5)

    # 绘图按钮
    btn_draw = tk.Button(root,
                         text="绘图",
                         cursor="hand2",
                         width=10,
                         bg="white",
                         relief="raised",
                         command=plot_f
                         )
    btn_draw.place(relx=0.4, rely=0.5)

    # 清除按钮
    btn_clear = tk.Button(root,
                          text="清除",
                          cursor="hand2",
                          width=10,
                          bg="white",
                          relief="raised",
                          command=clear
                          )
    btn_clear.place(relx=0.7, rely=0.5)

"""
    主程序文件，负责程序的启动与结束和窗体的大致设置。
"""

def win_w_h(root):
    """
        控制窗口的大小和出现的位置
    :param root:
    :return: 窗口的大小和出现的位置
    """
    # 设置标题：
    win.title("Sense Data")

    # 绘图区标签
    label_plot = tk.Label(root, text="绘     图       区",
                          font=("微软雅黑", 20), fg="blue")
    label_plot.place(relx=0.26, rely=0)

    label_func = tk.Label(root, text="选      择      区",
                          font=("微软雅黑", 20), fg="blue")
    label_func.place(relx=0.75, rely=0)
    # 获取屏幕的大小;
    screen_height = root.winfo_screenheight()
    screen_width = root.winfo_screenwidth()
    # 窗体的大小
    win_width = 0.8 * screen_width
    win_height = 0.8 * screen_height
    # 窗体出现的位置：控制的是左上角的坐标
    show_width = (screen_width - win_width) / 2
    show_height = (screen_height - win_height) / 2

    # 返回窗体 坐标
    return win_width, win_height, show_width, show_height

"""
    消息队列：接受传输数据
"""


def _async_raise(tid, exctype):
    """raises the exception, performs cleanup if needed"""
    tid = ctypes.c_long(tid)
    if not inspect.isclass(exctype):
        exctype = type(exctype)
    res = ctypes.pythonapi.PyThreadState_SetAsyncExc(tid, ctypes.py_object(exctype))
    if res == 0:
        raise ValueError("invalid thread id")
    elif res != 1:
        # """if it returns a number greater than one, you're in trouble,
        # and you should call it again with exc=NULL to revert the effect"""
        ctypes.pythonapi.PyThreadState_SetAsyncExc(tid, None)
        raise SystemError("PyThreadState_SetAsyncExc failed")


def stop_thread(thread):
    _async_raise(thread.ident, SystemExit)


def main(plt):
    T = threading.Thread(target = __main(plt))
    tid = T
    T.start()

def __main(plt):
    connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()

    channel.exchange_declare(exchange='direct_logs', exchange_type='direct')

    result = channel.queue_declare(queue='', exclusive=True)
    queue_name = result.method.queue

    # severities = sys.argv[1:]
    # if not severities:
    #     sys.stderr.write("Usage: %s [info] [warning] [error]\n" % sys.argv[0])
    #     sys.exit(1)

    for routing_key in routing_keys:
        channel.queue_bind(
            exchange='direct_logs', queue=queue_name, routing_key=routing_key)

    print(' [*] Waiting for logs. To exit press CTRL+C')
    def callback(ch, method, properties, body):
        #print(" [x] %r:%r" % (method.routing_key, body.decode()))
        if method.routing_key != "end":
            str1 = float(body.split()[1])
            str0 = bytes.decode(body.split()[0][5:])
            dict.setdefault(method.routing_key, []).append(str1)
            date.setdefault(method.routing_key, []).append(str0)
        else:
            display(plt)
    channel.basic_consume(
        queue=queue_name, on_message_callback=callback, auto_ack=True)
    channel.start_consuming()


def display(plt):
    # for key in dict.keys():
    #     print(key)
    #     print(dict[key])
    for key in date.keys():
        print(key)
        print(date[key])
        print(dict[key])
        plt.plot(date[key],dict[key],label=key)
    plt.set_xlabel('month')
    plt.set_ylabel('number')
    plt.set_title('Data Collection')
    plt.grid(True)
    plt.legend(loc = 'upper left')
    stop_thread(tid)
    print("stoped")


if __name__ == '__main__':

    win = tk.Tk()
    # 大小 位置
    win.geometry("%dx%d+%d+%d" % (win_w_h(win)))

    date = {}
    dict = {}
    routing_keys = ['end']


    # 创建一个容器, 没有画布时的背景
    frame1 = tk.Frame(win, bg="#c0c0c0")
    frame1.place(relx=0.00, rely=0.05, relwidth=0.62, relheight=0.89)


    # 控件区
    frame2 = tk.Frame(win, bg="#808080")
    frame2.place(relx=0.62, rely=0.05, relwidth=0.38, relheight=0.89)

    # 调用控件模块
    widget_main(win, frame2)
    win.mainloop()