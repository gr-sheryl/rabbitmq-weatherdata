#!/usr/bin/env python
import pika,csv
import sys,os

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.exchange_declare(exchange='direct_logs', exchange_type='direct')

#ordingary
# severity = sys.argv[1] if len(sys.argv) > 2 else 'info'
# message = ' '.join(sys.argv[2:]) or 'Hello World!'

#new
path = r'../data'
routing_key = ""
message = ""
for file in os.listdir(path):
    if file.endswith(".csv"):
        routing_key = file[:-4]
        print(routing_key)
        with open(os.path.join(path,file),'r',encoding='gbk') as f:
            csv_reader = csv.reader(f)
            for line in csv_reader:
                #只选取了北京地区2019年的数据
                if (line[0] == "54511") and (line[2][:4] == "2019"):
                    print(line)
                    message = line[2] + ' ' + line[3]
                    channel.basic_publish(exchange='direct_logs', routing_key=routing_key, body=message)
                    print(" [x] Sent %r:%r" % (routing_key, message))
routing_key="end"
message = '2021 923'
channel.basic_publish(exchange='direct_logs', routing_key=routing_key, body=message)
print(" [x] Sent %r:%r" % (routing_key, message))

connection.close()