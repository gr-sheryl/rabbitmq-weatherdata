#!/usr/bin/env python
import pika, sys, os
import matplotlib.pyplot as plt


date = {}
dict = {}
routing_keys = ['rain', 'humidity', 'sunshine','pressure','temperature', 'end']
#routing_keys = ['pressure','end']

def main():
    connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()

    channel.exchange_declare(exchange='direct_logs', exchange_type='direct')

    result = channel.queue_declare(queue='', exclusive=True)
    queue_name = result.method.queue

    # severities = sys.argv[1:]
    # if not severities:
    #     sys.stderr.write("Usage: %s [info] [warning] [error]\n" % sys.argv[0])
    #     sys.exit(1)

    for routing_key in routing_keys:
        channel.queue_bind(
            exchange='direct_logs', queue=queue_name, routing_key=routing_key)

    print(' [*] Waiting for logs. To exit press CTRL+C')
    def callback(ch, method, properties, body):
        #print(" [x] %r:%r" % (method.routing_key, body.decode()))
        if method.routing_key != "end":
            str1 = float(body.split()[1])
            str0 = bytes.decode(body.split()[0][5:])
            dict.setdefault(method.routing_key, []).append(str1)
            date.setdefault(method.routing_key, []).append(str0)
        else:
            display()
    channel.basic_consume(
        queue=queue_name, on_message_callback=callback, auto_ack=True)
    channel.start_consuming()


def display():
    # for key in dict.keys():
    #     print(key)
    #     print(dict[key])
    for key in date.keys():
        print(key)
        print(date[key])
        print(dict[key])
        plt.plot(date[key],dict[key],label=key)    #画第一条线
    plt.xlabel('month')
    plt.ylabel('number')
    plt.title('Data Collection')
    plt.legend(loc = 'upper left')
    plt.show()
if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)